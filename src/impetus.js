"use strict";

import { getWindow } from "ssr-window";

// Add momentum to anything. It's like iScroll, except not for scrolling. Supports mouse and touch events.
// https://github.com/chrisbateman/impetus

// fixes weird safari 10 bug where preventDefault is prevented
// @see https://github.com/metafizzy/flickity/issues/457#issuecomment-254501356
// const window=getWindow();
// window.addEventListener('touchmove', function () { });

export default class Impetus {
    constructor({
        source: sourceEl = document,
        update: updateCallback,
        multiplier = 1.25,
        friction = 0.94,
        boundX,
        boundY,
        touch = false,
        mouse = true,
        down
    }) {
        var boundXmin, boundXmax, boundYmin, boundYmax, pointerLastX, pointerLastY, pointerCurrentX, pointerCurrentY, pointerId;
        var targetX = 0;
        var targetY = 0;
        var targetBaseX = 0;
        var targetBaseY = 0;
        var ticking = false;
        var pointerActive = false;
        var paused = false;
        var decelerating = false;
        var trackingPoints = [];
        var first_event;
        var decTs, decVelX, decVelY;

        /**
         * Initialize instance
         */
        (function init() {
            sourceEl = (typeof sourceEl === 'string') ? document.querySelector(sourceEl) : sourceEl;
            if (!sourceEl) {
                throw new Error('IMPETUS: source not found.');
            }

            if (!updateCallback) {
                throw new Error('IMPETUS: update function not defined.');
            }

            // Initialize bound values
            if (boundX) {
                boundXmin = boundX[0];
                boundXmax = boundX[1];
            }
            if (boundY) {
                boundYmin = boundY[0];
                boundYmax = boundY[1];
            }

            touch && sourceEl.addEventListener('touchstart', onDown);
            mouse && sourceEl.addEventListener('mousedown', onDown);

            // sourceEl.onpointerdown=(e)=>{
            //     console.log(e);
            //     sourceEl.setPointerCapture(e.pointerId);
            // }

            // sourceEl.onpointerup=(e)=>{
            //     console.log(e);
            //     sourceEl.releasePointerCapture(e.pointerId);
            // }
        })();

        /**
         * In edge cases where you may need to
         * reinstanciate Impetus on the same sourceEl
         * this will remove the previous event listeners
         */
        this.destroy = function () {
            touch && sourceEl.removeEventListener('touchstart', onDown);
            mouse && sourceEl.removeEventListener('mousedown', onDown);

            cleanUpRuntimeEvents();

            // however it won't "destroy" a reference
            // to instance if you'd like to do that
            // it returns null as a convinience.
            // ex: `instance = instance.destroy();`
            return null;
        };

        /**
         * Disable movement processing
         * @public
         */
        this.pause = function () {
            cleanUpRuntimeEvents();

            pointerActive = false;
            paused = true;
        };

        /**
         * Enable movement processing
         * @public
         */
        this.resume = function () {
            paused = false;
        };

        /**
         * Update the current x and y values
         * @public
         * @param {Number} x
         * @param {Number} y
         */
        this.setValues = function (x, y, baseX, baseY) {
            if (typeof x === 'number') {
                targetX = x;
                if (typeof baseX === 'number')
                    targetBaseX = baseX;
            }
            if (typeof y === 'number') {
                targetY = y;

                if (typeof baseY === 'number')
                    targetBaseY = baseY;
            }
        };

        /**
         * Update the multiplier value
         * @public
         * @param {Number} val
         */
        this.setMultiplier = function (val) {
            multiplier = val;
        };

        /**
         * Update boundX value
         * @public
         * @param {Number[]} boundX
         */
        this.setBoundX = function (boundX) {
            boundXmin = boundX[0];
            boundXmax = boundX[1];
        };

        /**
         * Update boundY value
         * @public
         * @param {Number[]} boundY
         */
        this.setBoundY = function (boundY) {
            boundYmin = boundY[0];
            boundYmax = boundY[1];
        };

        /**
         * Removes all events set by this instance during runtime
         */
        function cleanUpRuntimeEvents() {
            // Remove all touch events added during 'onDown' as well.
            touch && sourceEl.removeEventListener('touchmove', onMove, getPassiveSupported() ? { passive: true } : true);
            touch && sourceEl.removeEventListener('touchend', onUp);
            touch && sourceEl.removeEventListener('touchcancel', stopTracking);
            mouse && sourceEl.removeEventListener('mousemove', onMove, getPassiveSupported() ? { passive: true } : true);
            mouse && sourceEl.removeEventListener('mouseup', onUp);
        }

        /**
         * Add all required runtime events
         */
        function addRuntimeEvents() {
            cleanUpRuntimeEvents();

            // @see https://developers.google.com/web/updates/2017/01/scrolling-intervention
            touch && sourceEl.addEventListener('touchmove', onMove, getPassiveSupported() ? { passive: true } : true);
            touch && sourceEl.addEventListener('touchend', onUp);
            touch && sourceEl.addEventListener('touchcancel', stopTracking);
            mouse && sourceEl.addEventListener('mousemove', onMove, getPassiveSupported() ? { passive: true } : true);
            mouse && sourceEl.addEventListener('mouseup', onUp);
        }

        /**
         * Executes the update function
         */
        function callUpdateCallback() {
            updateCallback.call(sourceEl, targetX, targetY);
        }

        /**
         * Creates a custom normalized event object from touch and mouse events
         * @param  {Event} ev
         * @returns {Object} with x, y, and id properties
         */
        function normalizeEvent(ev) {
            if (ev.type === 'touchmove' || ev.type === 'touchstart' || ev.type === 'touchend') {
                var touch = ev.targetTouches[0] || ev.changedTouches[0];
                return {
                    x: touch.clientX + targetBaseX,
                    y: touch.clientY + targetBaseY,
                    id: touch.identifier
                };
            } else { // mouse events
                return {
                    x: ev.clientX + targetBaseX,
                    y: ev.clientY + targetBaseY,
                    id: null
                };
            }
        }

        /**
         * Initializes movement tracking
         * @param  {Object} ev Normalized event
         */
        function onDown(ev) {
            if (!pointerActive && !paused) {
                
                down && down.call(sourceEl, ev);

                var event = normalizeEvent(ev);

                first_event = event;

                pointerActive = true;
                decelerating = false;
                pointerId = event.id;

                pointerLastX = pointerCurrentX = event.x;
                pointerLastY = pointerCurrentY = event.y;
                trackingPoints = [];
                addTrackingPoint(pointerLastX, pointerLastY);

                addRuntimeEvents();
            }
        }

        /**
         * Handles move events
         * @param  {Object} ev Normalized event
         */
        function onMove(ev) {
            // ev.preventDefault();
            var event = normalizeEvent(ev);
            if (pointerActive && event.id === pointerId) {
                event.x = 2 * first_event.x - event.x;
                event.y = 2 * first_event.y - event.y;

                pointerCurrentX = event.x;
                pointerCurrentY = event.y;
                addTrackingPoint(pointerLastX, pointerLastY);
                requestTick();
            }
        }

        /**
         * Handles up/end events
         * @param {Object} ev Normalized event
         */
        function onUp(ev) {
            var event = normalizeEvent(ev);

            if (pointerActive && event.id === pointerId) {
                stopTracking();
            }
        }

        /**
         * Stops movement tracking, starts animation
         */
        function stopTracking() {
            pointerActive = false;
            addTrackingPoint(pointerLastX, pointerLastY);

            startDecelAnim();

            cleanUpRuntimeEvents();
        }

        /**
         * Records movement for the last 100ms
         * @param {number} x
         * @param {number} y [description]
         */
        function addTrackingPoint(x, y) {
            var time = Date.now();
            while (trackingPoints.length > 0) {
                if (time - trackingPoints[0].time <= 100) {
                    break;
                }
                trackingPoints.shift();
            }

            trackingPoints.push({ x, y, time });
        }

        /**
         * Calculate new values, call update function
         */
        function updateAndRender() {
            var pointerChangeX = pointerCurrentX - pointerLastX;
            var pointerChangeY = pointerCurrentY - pointerLastY;

            targetX += pointerChangeX * multiplier;
            targetY += pointerChangeY * multiplier;

            targetX = targetX > 0 ? Math.floor(targetX) : Math.ceil(targetX);
            targetY = targetY > 0 ? Math.floor(targetY) : Math.ceil(targetY);

            checkBounds(true);

            callUpdateCallback();

            pointerLastX = pointerCurrentX;
            pointerLastY = pointerCurrentY;
            ticking = false;
        }

        /**
         * prevents animating faster than current framerate
         */
        function requestTick() {
            if (!ticking) {
                requestAnimFrame(updateAndRender);
            }
            ticking = true;
        }

        /**
         * Determine position relative to bounds
         * @param {Boolean} restrict Whether to restrict target to bounds
         */
        function checkBounds(restrict) {
            var xDiff = 0;
            var yDiff = 0;

            if (boundXmin !== undefined && targetX <= boundXmin) {
                xDiff = boundXmin - targetX;
            } else if (boundXmax !== undefined && targetX >= boundXmax) {
                xDiff = boundXmax - targetX;
            }

            if (boundYmin !== undefined && targetY <= boundYmin) {
                yDiff = boundYmin - targetY;
            } else if (boundYmax !== undefined && targetY >= boundYmax) {
                yDiff = boundYmax - targetY;
            }

            if (restrict) {
                if (xDiff !== 0) {
                    targetX = (xDiff > 0) ? boundXmin : boundXmax;
                }
                if (yDiff !== 0) {
                    targetY = (yDiff > 0) ? boundYmin : boundYmax;
                }
            }

            return {
                x: xDiff,
                y: yDiff,
                inBounds: xDiff === 0 && yDiff === 0
            };
        }

        /**
         * Initialize animation of values coming to a stop
         */
        function startDecelAnim() {
            var firstPoint = trackingPoints[0];
            var lastPoint = trackingPoints[trackingPoints.length - 1];

            var xOffset = lastPoint.x - firstPoint.x;
            var yOffset = lastPoint.y - firstPoint.y;
            var timeOffset = lastPoint.time - firstPoint.time;
            if (!timeOffset) return;

            var velX = Math.floor((xOffset / timeOffset) * 1000);
            var velY = Math.floor((yOffset / timeOffset) * 1000);

            decelerating = true;

            decVelX = velX;
            decVelY = velY;
            decTs = Date.now();

            stepDecelAnim();
        }

        /**
         * Animates values slowing down
         */
        function stepDecelAnim() {
            if (!decelerating) return;

            let now = Date.now();
            let elapsed = now - decTs;

            if (!elapsed) return requestAnimFrame(stepDecelAnim);

            decTs = now;

            decVelX = decVelX * friction;
            decVelY = decVelY * friction;

            decVelX = decVelX > 0 ? Math.floor(decVelX) : Math.ceil(decVelX);
            decVelY = decVelY > 0 ? Math.floor(decVelY) : Math.ceil(decVelY);

            let distX = (decVelX / 1000) * elapsed;
            let distY = (decVelY / 1000) * elapsed;

            distX = distX > 0 ? Math.floor(distX) : Math.ceil(distX);
            distY = distY > 0 ? Math.floor(distY) : Math.ceil(distY);

            targetX += distX;
            targetY += distY;

            var bounds = checkBounds();

            // normalize X
            if (bounds.x !== 0) {
                if (bounds.x > 0) targetX = boundXmin;
                else targetX = boundXmax;
            }

            // normalize Y
            if (bounds.y !== 0) {
                if (bounds.y > 0) targetY = boundYmin;
                else targetY = boundYmax;
            }

            callUpdateCallback();

            requestAnimFrame(stepDecelAnim);

            if ((distX === 0 && distY === 0) || (bounds.x !== 0 && bounds.y !== 0))
                decelerating = false;
        }
    }
}

/**
 * @see http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
 */
const requestAnimFrame = (function () {
    const window=getWindow();
    return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function (callback) {
        window.setTimeout(callback, 1000 / 60);
    };
})();


function getPassiveSupported() {
    const window=getWindow();
    let passiveSupported = false;

    try {
        var options = Object.defineProperty({}, "passive", {
            get: function () {
                passiveSupported = true;
            }
        });

        window.addEventListener("test", null, options);
    } catch (err) { }

    getPassiveSupported = () => passiveSupported;
    return passiveSupported;
}