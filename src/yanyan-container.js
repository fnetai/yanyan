"use strict";

import React, { useRef, useEffect } from "react";
import { getWindow } from "ssr-window";

import "./yanyan.scss";
import Impetus from "./impetus";
import utils from "./yanyan-utils";
import { compute } from 'compute-scroll-into-view'
import { animate } from 'popmotion';
import YanyanChannel from './yanyan-channel';

export default ({ children, storePath, autoScroll }) => {
  const ref = useRef(null);
  const impetusRef = useRef(null);

  useEffect(() => {
    let ticking = false;
    let momentum_start;
    let scroll_left_prev;
    let is_scrolling = false;
    let is_scrolling_timeout_id = -1;
    let user_started_scroll = false;
    let user_started_scroll_left;
    let playback = [];
    const channel = YanyanChannel.connect(storePath);
    let update_queue = [];
    let keydown_queue = [];

    const update_dir = (x) => {
      if (!ref.current) return;
      const container = ref.current;
      container.setAttribute("data-sdx", x);
    };

    const _scroll_to_slide = (dirX) => {
      if (!ref.current) return;

      const container = ref.current;
      const wrapper = container.querySelector(".yanyan-wrapper");
      const sdx_prev = typeof dirX !== 'undefined' ? dirX : utils.getNumberAttr(container, 'data-sdx');

      // AUTOMATIC SCROLL
      if (autoScroll && (sdx_prev === -1 || sdx_prev === 1)) {
        const slides = wrapper.querySelectorAll("[data-bxo='1']");
        if (slides.length > 1) {
          const aleftx = slides[0].getBoundingClientRect().left;
          const bleftx = slides[1].getBoundingClientRect().left;
          const slide = sdx_prev === -1 ? aleftx < bleftx ? slides[0] : slides[1] : aleftx < bleftx ? slides[1] : slides[0];
          // console.log(aleftx, bleftx);
          // scrollIntoView(slide, {
          //   ease: t => (1 + Math.sin(Math.PI * t - Math.PI / 2)) / 2,
          //   duration: 250
          // });
          // scrollIntoView(slide, {
          //   duration: 300,
          //   behavior: instructions => {
          //     const [{ el, left }] = instructions;
          //     const elStyler = styler(el);
          //     animate({ from: el.scrollLeft, to: left }).start(left =>
          //       elStyler.set('scrollLeft', left)
          //     );
          //   },
          // });
        }
      }
    }

    const set_is_scrolling_timeout = () => {
      is_scrolling = true;
      if (is_scrolling_timeout_id) clearTimeout(is_scrolling_timeout_id);
      is_scrolling_timeout_id = setTimeout(reset_is_scrolling, 250);
    };

    const reset_is_scrolling = () => {
      is_scrolling = false;
      is_scrolling_timeout_id = -1;
      update_dir(0);
    };

    const request_update_tick = (event, reason) => {
      if (!update_queue.length) {
        requestAnimFrame(() => {
          update(event, reason);

          if (update_queue.length) {
            const last = update_queue[update_queue.length - 1];
            update_queue = [];
            request_update_tick(last[0], last[1]);
          }

        });
      }
      else update_queue.push([event, reason]);
    };

    const push_keydown_to_queue = (event) => {

      if (event.key === 'ArrowLeft' || (event.key === 'Tab' && event.shiftKey)) {
        event.preventDefault();
      }
      else if (event.key === 'ArrowRight' || event.key === 'Tab') {
        event.preventDefault();
      }
      else if (event.key === 'ArrowDown') {
        event.preventDefault();
      }
      else if (event.key === 'ArrowUp') {
        event.preventDefault();
      }
      else return;

      // console.log(keydown_queue.length);

      keydown_queue.push(event);
    };

    const request_keydown_tick = () => {

      requestAnimFrame(() => {
        if (keydown_queue.length) {
          const last = keydown_queue[keydown_queue.length - 1];
          keydown_queue = [];
          handle_keydown(last);
        }
        setTimeout(request_keydown_tick, 100);
      });
    };

    const update = (event, reason) => {
      if (!ref.current) return;

      set_is_scrolling_timeout();

      const container = ref.current;
      const impetus = impetusRef.current;

      if (typeof scroll_left_prev === "undefined") {
        scroll_left_prev = container.scrollLeft;
        update_dir(0);
      } else {
        const scroll_delta_left = container.scrollLeft - scroll_left_prev;
        const dirX = scroll_delta_left === 0 ? 0 : scroll_delta_left > 0 ? 1 : -1;
        scroll_left_prev = container.scrollLeft;
        update_dir(dirX);
      }

      if (impetus) {
        impetus.setBoundX([0, container.scrollWidth]);
        impetus.setBoundY([0, container.scrollHeight]);
      }

      channel && channel.emit("update", reason);
    };

    const init_momentum = () => {
      if (impetusRef.current) impetusRef.current.destroy();

      const container = ref.current;

      impetusRef.current = new Impetus({
        source: container,
        boundX: [0, container.scrollWidth],
        boundY: [0, container.scrollHeight],
        touch: false,
        mouse: true,
        friction: autoScroll ? 0 : 0.96,
        no_decay: autoScroll,

        down: (event) => {
          const impetus = impetusRef.current;

          momentum_start = {
            baseX: container.scrollLeft,
            baseY: container.scrollTop,
            x: container.scrollLeft + event.x,
            y: container.scrollTop + event.y,
          };

          impetus.setBoundX([0, container.scrollWidth]);
          impetus.setBoundY([0, container.scrollHeight]);
          impetus.setValues(momentum_start.x, momentum_start.y, momentum_start.baseX, momentum_start.baseY);
        },

        update: (targetX, targetY) => {
          if (!container) return;

          let scrollDeltaX = targetX - momentum_start.x;
          let scrollDeltaY = targetY - momentum_start.y;
          container.scrollLeft = momentum_start.baseX + scrollDeltaX;
          container.scrollTop = momentum_start.baseY + scrollDeltaY;
        },
      });
    };

    const init_container = (event) => {
      // request_update_tick(event, "init");
    };

    const handle_resize = (event) => {
      const container = ref.current;
      if (container) {
        container.scrollLeft = 0;
        container.scrollTop = 0;
      }

      request_update_tick(event, "resize");
    };

    const handle_orientation = (event) => {
      request_update_tick(event, "orientation");
    };

    const handle_scroll = (event) => {
      request_update_tick(event, "scroll");
    };

    const handle_scroll_start = (event) => {
      user_started_scroll_left = ref.current.scrollLeft;
      user_started_scroll = true;
    };

    const handle_scroll_end = (event) => {
      const container = ref.current;

      user_started_scroll = false;
      const scroll_delta_left = container.scrollLeft - user_started_scroll_left;
      const dirX = scroll_delta_left === 0 ? 0 : scroll_delta_left > 0 ? 1 : -1;
      if (autoScroll) {
        container.style['overflow-x'] = 'hidden';
        requestAnimFrame(() => container.style['overflow-x'] = '', 0);
        _scroll_to_slide(dirX);
      }
    };

    const handle_mouse_wheel = (event) => {
      Math.floor(Math.abs(event.deltaX)) !== 0 && event.preventDefault();
    }

    const stop_animations = () => {
      // console.log(playback.length);
      playback.forEach(anim => {
        // console.log(anim);
        anim.stop();
      });
      playback = [];
    }

    channel.scroll_to_slide = (slide, block) => {

      // console.log(block);

      stop_animations();

      const actions = compute(slide, {
        scrollMode: 'always',
        block: 'nearest',
        inline: "nearest",
      });

      // console.log(actions.length);

      actions.forEach(({ el, top, left }) => {
        // horizontal
        // console.log(top,left);

        const animX = animate({
          from: el.scrollLeft,
          to: left,
          onUpdate: (latest) => {
            el.scrollLeft = latest;
          },
        });
        playback.push(animX);

        // vertical
        const animY = animate({
          from: el.scrollTop,
          to: top,
          onUpdate: (latest) => {
            el.scrollTop = latest;
          },
        });
        playback.push(animY);
      });

      // slide.focus();
      slide.focus({ preventScroll: true });
    }

    const handle_keydown = (event) => {

      const container = ref.current;

      if (event.key === 'ArrowLeft' || (event.key === 'Tab' && event.shiftKey)) {
        event.preventDefault();
        channel.focus_back({ trigger: "keydown" });
      }
      else if (event.key === 'ArrowRight' || event.key === 'Tab') {
        event.preventDefault();
        channel.focus_forward({ trigger: "keydown" });
      }
      else if (event.key === 'ArrowDown') {
        event.preventDefault();
        const yanyan = container.parentElement;
        const yanyan_next = yanyan.nextSibling;
        const channel_path = yanyan_next?.getAttribute('data-channel');
        if (!channel_path) return;

        const channel = YanyanChannel.connect(channel_path);
        stop_animations();
        channel.focus();
      }
      else if (event.key === 'ArrowUp') {
        event.preventDefault();
        const yanyan = container.parentElement;
        const yanyan_next = yanyan.previousSibling;
        const channel_path = yanyan_next?.getAttribute('data-channel');
        if (!channel_path) return;

        const channel = YanyanChannel.connect(channel_path);
        stop_animations();
        channel.focus();
      }
    }

    channel.focus = () => {
      channel.focus_forward({ nearest_to_active: true });
    }

    if (ref.current) {
      // init_container();
      init_momentum();

      const container = ref.current;

      // resize and scroll
      getWindow().addEventListener("resize", handle_resize, { passive: true });
      getWindow().addEventListener("orientationchange", handle_orientation, { passive: true });

      container.addEventListener("scroll", handle_scroll, { passive: true });
      container.addEventListener('keydown', handle_keydown, { passive: false });
      // container.addEventListener("wheel", handle_mouse_wheel, { passive: false });

      if (autoScroll) {
        container.addEventListener("mousedown", handle_scroll_start, { passive: true });
        container.addEventListener("touchstart", handle_scroll_start, { passive: true });
        container.addEventListener("touchend", handle_scroll_end, { passive: true });
        container.addEventListener("mouseup", handle_scroll_end, { passive: true });
      }

      // request_keydown_tick();
    }

    return () => {
      const container = ref.current;

      if (container) {

        const window = getWindow();

        window.removeEventListener("resize", handle_resize);
        window.removeEventListener("orientationchange", handle_orientation);

        container.removeEventListener("scroll", handle_scroll);
        container.removeEventListener("keydown", push_keydown_to_queue);
        // container.removeEventListener("wheel", handle_mouse_wheel);

        if (autoScroll) {
          container.removeEventListener("mousedown", handle_scroll_start);
          container.removeEventListener("touchstart", handle_scroll_start);
          container.removeEventListener("touchend", handle_scroll_end);
          container.removeEventListener("mouseup", handle_scroll_end);
        }
      }
      if (impetusRef.current) {
        impetusRef.current.destroy();
      }
    };
  }, [ref]);

  return (
    <div className="yanyan-container" ref={ref}>
      {children}
    </div>
  );
};

/**
 * @see http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
 */
const requestAnimFrame = (function () {
  const window = getWindow();
  return (
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    function (callback) {
      window.setTimeout(callback, 1000 / 60);
    }
  );
})();