"use strict";

import React, { useRef, useEffect } from "react";
import './yanyan.scss';
import utils from './yanyan-utils';
import YanyanChannel from './yanyan-channel';

export default ({ children, storePath }) => {
    const ref = useRef(null);

    useEffect(() => {
        const wrapper = ref.current;
        const container = wrapper?.parentElement;
        const channel = YanyanChannel.connect(storePath);

        const scan = (debug) => {
            let next = wrapper.firstChild;

            let nonOverlapped = true;
            let allContained = true;

            let view_stack = [];
            let view_splitted = false;
            let view_splitted_index = -1;
            let view_first, view_last;
            let view_slide_focused;

            while (next) {
                // const lpc = utils.getNumberAttr(next, 'data-lpc', 0);
                const bxo = utils.getNumberAttr(next, 'data-bxo', 0);
                const bxc = utils.getNumberAttr(next, 'data-bxc', 0);

                debug && console.log(bxo);

                nonOverlapped &= (bxo === 1);
                allContained &= (bxc === 1);

                if (document.activeElement === next) view_slide_focused = next;

                if (bxo === 1) {
                    if (view_splitted) {
                        view_splitted_index++;
                        view_stack.splice(view_splitted_index, 0, next);
                    }
                    else view_stack.push(next);
                } else {
                    if (view_stack.length) view_splitted = true;
                }

                next = next.nextSibling;
            }

            view_first = view_stack[0];
            view_last = view_stack[view_stack.length - 1];

            return {
                nonOverlapped,
                allContained,
                view_first,
                view_last,
                view_stack,
                view_slide_focused
            };
        }

        const onUpdate = (reason) => {

            // console.log(reason);

            const status = scan();

            container.setAttribute('data-nop', status.nonOverlapped ? 1 : 0);
            container.setAttribute('data-acd', status.allContained ? 1 : 0);

            // IS BACK ENABLED
            const nav_back_enabled = channel.nav_back_enabled;
            channel.nav_back_enabled = container.scrollLeft !== 0 && !status.allContained;
            nav_back_enabled !== channel.nav_back_enabled && channel.emit('nav_back_enabled', channel.nav_back_enabled);

            // IS FORWARD ENABLED
            const nav_forward_enabled = channel.nav_forward_enabled;
            channel.nav_forward_enabled = !status.allContained;
            nav_forward_enabled !== channel.nav_forward_enabled && channel.emit('nav_forward_enabled', channel.nav_forward_enabled);
        }

        channel.back = () => {
            const status = scan();

            // console.log('BACK', status.view_stack.length);

            channel.scroll_to_slide(status.view_first, "end");
        }

        channel.forward = () => {
            const status = scan();

            // console.log('FORWARD', status.view_stack.length);

            channel.scroll_to_slide(status.view_last, "start");
        }

        channel.focus_forward = (params) => {
            // console.log(params);
            const status = scan();

            if (params?.nearest_to_active && document.activeElement) {
                const ai_bcr = document.activeElement.getBoundingClientRect();
                const ai_center = [ai_bcr.x + ai_bcr.width / 2, ai_bcr.x + ai_bcr.height / 2];

                let target;
                status.view_stack.forEach(item => {
                    const vi_bcr = item.getBoundingClientRect();
                    const vi_center = [vi_bcr.x + vi_bcr.width / 2, vi_bcr.x + vi_bcr.height / 2];
                    const dist = utils.getDistance(ai_center[0], ai_center[1], vi_center[0], vi_center[1]);
                    if (!target) target = [item, dist];
                    else {
                        if (target[1] > dist) target = [item, dist];
                    }
                });

                if (target) {
                    channel.scroll_to_slide(target[0], "start");
                    return;
                }
                // console.log(ai_center);
            }

            let nextFocus;
            if (status.view_slide_focused) nextFocus = status.view_slide_focused.nextSibling || wrapper.firstChild;
            else nextFocus = status.view_stack[0];

            if (nextFocus) channel.scroll_to_slide(nextFocus, "start");
        }

        channel.focus_back = () => {
            const status = scan();

            let nextFocus;
            if (status.view_slide_focused) nextFocus = status.view_slide_focused.previousSibling || wrapper.lastChild;
            else nextFocus = status.view_stack[status.view_stack.length - 1];

            if (nextFocus) channel.scroll_to_slide(nextFocus, "start");
        }

        channel.on('update', onUpdate);

        return () => {
            channel.removeListener('update', onUpdate);
        }
    }, [ref]);
    return (
        <div ref={ref} className='yanyan-wrapper'>{children}</div>
    )
}