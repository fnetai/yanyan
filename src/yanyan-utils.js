"use strict";
// Check if rectangle a contains rectangle b
// Each object (a and b) should have 2 properties to represent the
// top-left corner (x1, y1) and 2 for the bottom-right corner (x2, y2).
function contains(a, b) {
    return !(
        b.top < a.top ||
        b.left < a.left ||
        b.bottom > a.bottom ||
        b.right > a.right
    );
}

// Check if rectangle a overlaps rectangle b
// Each object (a and b) should have 2 properties to represent the
// top-left corner (x1, y1) and 2 for the bottom-right corner (x2, y2).
function overlaps(a, b) {
    if (a.top >= b.bottom || b.top >= a.bottom) return false;

    // if ((a.left + 4) >= b.right || b.left >= (a.right - 4)) return false;
    if ((a.left) >= b.right || b.left >= (a.right)) return false;

    return true;
}

// Check if rectangle a touches rectangle b
// Each object (a and b) should have 2 properties to represent the
// top-left corner (x1, y1) and 2 for the bottom-right corner (x2, y2).
function touches(a, b) {
    // has horizontal gap
    if (a.top > b.bottom || b.top > a.bottom) return false;

    // has vertical gap
    if (a.left > b.right || b.left > a.right) return false;

    return true;
}

function side(a, b) {
    if (a.left > b.right) return -1;
    if (a.right < b.left) return 1;

    // if (a.left > b.right || a.bottom < b.top) return -1;

    // if (a.right < b.left || a.top < b.bottom) return 1;

    return 0;
}

const getNumberAttr = (el, key, defValue) => {
    let value = el.getAttribute(key);
    if (value === null || value === '') return defValue;
    else return Number.parseInt(value);
}

function uuid() {
    function chr4() {
        return Math.random().toString(16).slice(-4);
    }
    return chr4() + chr4() +
        '-' + chr4() +
        '-' + chr4() +
        '-' + chr4() +
        '-' + chr4() + chr4() + chr4();
}

function getDistance(x1, y1, x2, y2) {
    let y = x2 - x1;
    let x = y2 - y1;

    return Math.sqrt(x * x + y * y);
}

export default {
    contains,
    overlaps,
    touches,
    side,
    getNumberAttr,
    uuid,
    getDistance
}