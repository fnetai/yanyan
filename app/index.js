import React from "react";
import YanYan from "../src";
import GeoPattern from "geopattern";

const generate_style = ({ namespace, height }) => {
  // const color_hex = colorspace(namespace);
  // const color = Color(color_hex);
  const setBackgroundImage = (namespace) => {
    var pattern = GeoPattern.generate(namespace, {});
    return pattern.toDataUrl();
  }

  return {
    height: height || "20vh",
    // width: "80%",
    // width:"40px",
    // backgroundColor: color_hex,
    // backgroundColor: "#1f1f1f",
    border: "1px solid #141414",
    display: "flex",
    color: "#e0e0e0",
    fontSize: "5vw",
    // fontSize: "36px",
    // color: color.negate().hex(),
    alignContent: "center",
    justifyContent: "center",
    alignItems: "center",
    boxSizing: "border-box",
    backgroundImage: setBackgroundImage(namespace)
  }
}

export default function Grid() {
  return (
    <div style={{ width: "100%" }}>
      <YanYan className={"yy--col-fixed-100"}>
        {
          Array(33).fill(0).map((item, index) =>
            <div key={index}
              style={generate_style({ namespace: `1:${index}`, height: "65vh" })}
            >{`1:${index}`}</div>
          )
        }
      </YanYan>
      <YanYan className={"yy--col-fixed-50"}>
        {
          Array(33).fill(0).map((item, index) =>
            <div key={index}
              style={generate_style({ namespace: `2:${index}`, height: "25vh" })}
            >{`2:${index}`}</div>
          )
        }
      </YanYan>
      <YanYan className={"yy--col-fixed-25"}>
        {
          Array(33).fill(0).map((item, index) =>
            <div key={index}
              style={generate_style({ namespace: `3:${index}`, height: "30vh" })}
            >{`3:${index}`}</div>
          )
        }
      </YanYan>
      <YanYan className={"yy--col-fixed-12-5"}>
        {
          Array(33).fill(0).map((item, index) =>
            <div key={index}
              style={generate_style({ namespace: `4:${index}` })}
            >{`4:${index}`}</div>
          )
        }
      </YanYan>
      <YanYan className={"yy--col-fixed-100"}>
        {
          Array(33).fill(0).map((item, index) =>
            <div key={index}
              style={generate_style({ namespace: `5:${index}`, height: "40vh" })}
            >{`5:${index}`}</div>
          )
        }
      </YanYan>
      <YanYan className={"yy--col-fixed-50"}>
        {
          Array(33).fill(0).map((item, index) =>
            <div key={index}
              style={generate_style({ namespace: `6:${index}`, height: "30vh" })}
            >{`6:${index}`}</div>
          )
        }
      </YanYan>
      <YanYan className={"yy--col-fixed-25"}>
        {
          Array(33).fill(0).map((item, index) =>
            <div key={index}
              style={generate_style({ namespace: `7:${index}`, height: "25vh" })}
            >{`7:${index}`}</div>
          )
        }
      </YanYan>
      <YanYan className={"yy--col-fixed-20"}>
        {
          Array(33).fill(0).map((item, index) =>
            <div key={index}
              style={generate_style({ namespace: `8:${index}`, height: "50vh" })}
            >{`8:${index}`}</div>
          )
        }
      </YanYan>
    </div>
  )
}