"use strict";

import React, { useRef, useEffect } from "react";

import './yanyan.scss';
import utils from './yanyan-utils';
import YanyanChannel from './yanyan-channel';

export default ({ children, storePath, index }) => {
    const ref = useRef(null);

    useEffect(() => {
        const slide = ref.current;
        const channel = YanyanChannel.connect(storePath);

        const resetLayout = (reason) => {
            if (!slide) return;

            slide.style['transform'] = '';
            slide.style['webkitTransform'] = '';
            slide.removeAttribute('data-bxo');
            slide.removeAttribute('data-bxc');
            slide.removeAttribute('data-bxs');
            slide.removeAttribute('data-lpc');
            slide.removeAttribute('data-sdx');
        }
        
        const onUpdate = (reason) => {
            
            if (!slide) return;

            // NOT GOOD SOLUTION
            if (reason === 'resize' || reason === 'orientation' || reason === 'slide_data_changed')
                resetLayout(reason);

            const wrapper = slide.parentElement;
            const container = wrapper.parentElement;

            const slide_bcr = slide.getBoundingClientRect();
            const container_bcr = container.getBoundingClientRect();

            const sdx_container = utils.getNumberAttr(container, 'data-sdx', 0);
            const nop_container = utils.getNumberAttr(container, 'data-nop', 0);

            const bxc = utils.contains(container_bcr, slide_bcr);
            const bxo = utils.overlaps(container_bcr, slide_bcr);
            const bxs = utils.side(container_bcr, slide_bcr);

            const lpc = utils.getNumberAttr(slide, 'data-lpc', 0);

            bxc ? slide.setAttribute('data-bxc', 1) : slide.removeAttribute('data-bxc');
            bxo ? slide.setAttribute('data-bxo', 1) : slide.removeAttribute('data-bxo');
            slide.setAttribute('data-bxs', bxs);

            if (bxo) {
                const images = ref.current.querySelectorAll('img.yy--image-lazy[data-src]');
                images.forEach(image => {
                    image.src = image.getAttribute('data-src');
                    image.style.display = 'inline';
                });
            }

            if (sdx_container === 1 && !bxc && !bxo && !nop_container) {

                if (bxs === -1) {

                    const wrapper_bcr = wrapper.getBoundingClientRect();

                    const lpc_next_temp = lpc + 1;
                    const lpc_next = lpc_next_temp < 0 ? 0 : lpc_next_temp;

                    const translateX = lpc_next * wrapper_bcr.width;

                    slide.style['transform'] = `translateX(${translateX}px)`;
                    slide.style['webkitTransform'] = `translateX(${translateX}px)`;

                    slide.setAttribute("data-lpc", lpc_next);
                    slide.setAttribute("data-sdx", sdx_container);
                    slide.setAttribute("data-bxs", 0);
                }
            }
            else if (sdx_container === -1 && !bxc && !bxo && !nop_container) {

                if (bxs === 1) {

                    const wrapper_bcr = wrapper.getBoundingClientRect();

                    const lpc_next_temp = lpc - 1;
                    const lpc_next = lpc_next_temp < 0 ? 0 : lpc_next_temp;

                    const translateX = lpc_next * wrapper_bcr.width;
                    slide.style['transform'] = `translateX(${translateX}px)`;
                    slide.style['webkitTransform'] = `translateX(${translateX}px)`;

                    slide.setAttribute("data-lpc", lpc_next);
                    slide.setAttribute("data-sdx", sdx_container);
                    slide.setAttribute("data-bxs", 0);
                }
            }
        }

        const onMounted = (event) => {
            if (event.type !== 'slide' || event.index !== index) return;
            onUpdate('mounted', event);
        }

        // register to events
        slide && channel && channel.on('update', onUpdate);
        slide && channel && channel.on('mounted', onMounted);

        return () => {
            channel && channel.removeListener('update', onUpdate);
            channel && channel.removeListener('mounted', onMounted);
        }
    }, [ref]);

    return (
        <div ref={ref} tabIndex={0} className='yanyan-slide'>{children}</div>
    )
}