"use strict";

import EventEmitter from "eventemitter3";

const _cache = {};

export default class YanyanChannel extends EventEmitter {
    static create(id) {
        const channel = _cache[id];
        if (channel) return channel;
        _cache[id] = new YanyanChannel();
        return _cache[id];
    }
    static connect(id) {
        return _cache[id];
    }
    static destroy(id) {
        delete _cache[id];
    }
}