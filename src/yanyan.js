import React, { useState, useEffect, useRef } from "react";
import { Store, Action } from "@flownet/react-app-state";

import './yanyan.scss';
import YanyanContainer from "./yanyan-container";
import YanyanWrapper from "./yanyan-wrapper";
import YanyanSlide from "./yanyan-slide";
// import YanyanDebug from "./yanyan-debug";
import YanyanChannel from './yanyan-channel';

import utils from './yanyan-utils';
// import device from './utils/device'; // not used

export default ({ children, customStorePath, debug, className, autoScroll = false, slideData }) => {
    const ref = useRef(null);

    const [storePath] = useState(customStorePath || utils.uuid());
    const channel = useRef(YanyanChannel.create(storePath))?.current;

    useEffect(() => {
        channel.emit('update', 'slide_data_changed');
    }, [slideData]);

    useEffect(() => {
        return () => {
            YanyanChannel.destroy(storePath);
            Store.dispatch(Action.update(storePath));
        }
    }, []);

    useEffect(() => {
        if (ref.current) {
            // not used
            // device.ios && ref.current.setAttribute('data-ios', 1);
            // device.android && ref.current.setAttribute('data-android', 1);
            // device.macos && ref.current.setAttribute('data-macos', 1);
            // device.windows && ref.current.setAttribute('data-windows', 1);
            // device.iphone && ref.current.setAttribute('data-iphone', 1);
            // device.ipad && ref.current.setAttribute('data-ipad', 1);
            // device.edge && ref.current.setAttribute('data-edge', 1);
            // device.ie && ref.current.setAttribute('data-ie', 1);
            // device.firefox && ref.current.setAttribute('data-firefox', 1);
            ref.current.setAttribute('data-channel', storePath);
        }
    }, [ref])

    return (
        <div ref={ref} className={`yanyan ${className ? className : ''}`}>
            <YanyanContainer storePath={storePath} autoScroll={autoScroll}>
                <YanyanWrapper storePath={storePath} >
                    {
                        children && (Array.isArray(children) ? children : [children]).map((item, index) => (
                            <YanyanSlide key={index} index={index} storePath={storePath}>{item}</YanyanSlide>)
                        )
                    }
                </YanyanWrapper>
            </YanyanContainer>
        </div>
    )
}