"use strict";

import React from "react";
import useMediaQuery  from "@mui/material/useMediaQuery";

const Horizontal = ({ dataSrc, src }) => {
  const ref = React.useRef();
  const imageRef = React.useRef();
  const imageProcess = React.useRef({});
  const smallScreen = useMediaQuery("(max-width: 768px)");
  
  const onImageLoad = () => {
    reset();
  };
  const reset = () => {
    if (!imageRef.current) return;

    const container = ref.current;
    const img = imageRef.current;
    const process = imageProcess.current;

    // DIMENSIONS
    process.nwidth = img.naturalWidth;
    process.nheight = img.naturalHeight;
    process.width = img.offsetWidth;
    process.height = img.offsetHeight;
    process.cwidth = container.offsetWidth;
    process.cheight = container.offsetHeight;
    process.doit = process.cwidth > process.width;

    reset_translate();
    !process.doit && translate_image();
  };
  const reset_translate = () => {
    const img = imageRef.current;
    img.style["transform"] = "";
    img.style["webkitTransform"] = "";
  };
  const translate_image = () => {
    const img = imageRef.current;
    const process = imageProcess.current;
    const tx = -(process.width - process.cwidth) / 2;
    img.style["transform"] = `translateX(${tx}px)`;
    img.style["webkitTransform"] = `translateX(${tx}px)`;
  };
  const onOrientationChanged = () => {
    setTimeout(reset, 1000);
  };

  React.useEffect(() => {
    window.addEventListener("resize", reset, { passive: true });
    window.addEventListener("orientationchange", onOrientationChanged, {
      passive: true,
    });

    return () => {
      window.removeEventListener("resize", reset);
      window.removeEventListener("orientationchange", onOrientationChanged);
    };
  }, []);
  return (
    <div
      ref={ref}
      className="yanyan-image h-100 w-100 position-relative overflow-hidden"
      style={{ whiteSpace: "nowrap", padding: 0, margin: 0, lineHeight: 0 }}
    >
      <img
        className="d-inline-block yy--image-lazy"
        onLoad={onImageLoad}
        ref={imageRef}
        src={src}
        data-src={dataSrc}
        loading="lazy"
        style={{ width: "auto", height: "100%", pointerEvents: "none" }}
      />
      <img
        className="d-inline-block position-relative yy--image-lazy"
        src={src}
        data-src={dataSrc}
        loading="lazy"
        style={{
          left: "-1px",
          width: "auto",
          height: "100%",
          transform: "scaleX(-1)",
          pointerEvents: "none",
        }}
      />
      <img
        className="d-inline-block position-relative yy--image-lazy"
        src={src}
        data-src={dataSrc}
        loading="lazy"
        style={{
          left: "-2px",
          width: "auto",
          height: "100%",
          pointerEvents: "none",
        }}
      />
      <div
        className="position-absolute w-100 h-100"
        style={{
          left: 0,
          bottom: 0,
          backgroundImage: smallScreen
            ? "linear-gradient(to top, #0008 , #0006 , #0003, #0000)"
            : "radial-gradient(circle at bottom left,rgb(0,0,0,1),rgb(0,0,0,0.3),rgb(0,0,0,0))",
        }}
      />
    </div>
  );
};
const Vertical = ({ dataSrc, src }) => {
  const ref = React.useRef();
  const imageRef = React.useRef();
  const imageProcess = React.useRef({});
  const smallScreen = useMediaQuery("(max-width: 768px)");

  const onImageLoad = () => {
    reset();
  };
  const reset = () => {
    if (!imageRef.current) return;

    const container = ref.current;
    const img = imageRef.current;
    const process = imageProcess.current;

    // DIMENSIONS
    process.nwidth = img.naturalWidth;
    process.nheight = img.naturalHeight;
    process.width = img.offsetWidth;
    process.height = img.offsetHeight;
    process.cwidth = container.offsetWidth;
    process.cheight = container.offsetHeight;
    process.doit = process.cheight > process.height;
  };
  React.useEffect(() => {
    window.addEventListener("resize", reset, { passive: true });
    window.addEventListener("orientationchange", reset, { passive: true });

    return () => {
      window.removeEventListener("resize", reset);
      window.removeEventListener("orientationchange", reset);
    };
  }, []);
  return (
    <div
      ref={ref}
      className="yanyan-image h-100 w-100 position-relative overflow-hidden"
    >
      <img
        className="d-block yy--image-lazy"
        onLoad={onImageLoad}
        ref={imageRef}
        src={src}
        data-src={dataSrc}
        loading="lazy"
        style={{ width: "100%", height: "auto", pointerEvents: "none" }}
      />
      <img
        className="d-block position-relative yy--image-lazy"
        src={src}
        data-src={dataSrc}
        loading="lazy"
        style={{
          top: "-1px",
          width: "100%",
          height: "auto",
          transform: "scaleY(-1)",
          pointerEvents: "none",
        }}
      />
      <div
        className="position-absolute w-100 h-100"
        style={{
          left: 0,
          bottom: 0,
          backgroundImage: smallScreen
            ? "linear-gradient(to top, #0008 , #0006 , #0003, #0000)"
            : "radial-gradient(circle at bottom left,rgb(0,0,0,1),rgb(0,0,0,0.3),rgb(0,0,0,0))",
        }}
      />
    </div>
  );
};

export default (props) => {
  const { horizontal } = props;
  return horizontal ? <Horizontal {...props} /> : <Vertical {...props} />;
};
