"use strict";

import React from "react";
import './yanyan.scss';
import { useSelector } from "@flownet/react-app-state";

export default ({ storePath }) => {
    const view = useSelector(state => state.default?.[storePath]?.view || {});
    return (
        <div className="yanyan-debug">
            <div>dirX: <span>{view.sdx}</span> </div>
            {/* <div>start: <span>{view.slide_start}</span> </div> */}
            <div>slides: <span>{view.slides}</span> </div>
            <div>grid: <span>{view.columns}</span> </div>
            <div>x: <span>{view.x}</span> </div>
            <div>y: <span>{view.y}</span> </div>
            <div>w: <span>{view.w}</span> </div>
            <div>h: <span>{view.h}</span> </div>
            <div>mx: <span>{view.mx}</span></div>
            <div>my: <span>{view.my}</span></div>
        </div>
    )
}