"use strict";

import React from 'react'
import YanyanChannel from "./yanyan-channel";

export default ({ storepath, type, index }) => {
    React.useEffect(() => {
        const channel = YanyanChannel.connect(storepath);
        channel.emit('mounted', { type, index });
    }, []);
    return <></>
}